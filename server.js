// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var game_server = require('./server/SnakeGameServer.js');
var io = require('socket.io')(server);
//io.set('transports', ['xhr-polling']);
//io.set('polling duration', 10);
var port = process.env.PORT || 1337;
server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

// express routing
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/server/models'));

var players = [];
var defaultColors = [
    '#000000',
    '#FF0000',
    '#00FF00',
    '#0000FF',
    '#FFD700',
    '#00FFFF',
    '#FF00FF',
    '#800000',
    '#808000',
    '#008000',
    '#800080',
    '#008080',
    '#000080'
];
var colors = defaultColors.slice(0);
io.on('connection', function (socket) {
    var id = 0;
    socket.emit('colors', colors);
    socket.on('login', function (username, color) {
        var error = false;
        players.forEach(function (player) {
            if (player.name == username) {
                socket.emit('username taken', username);
                error = true;
            }
        })
        if (!error) {
            var i;
            colors.forEach(function (col, index) {
                if (col == color) {
                    i = index;
                }
            });
            if (i)colors.splice(i, 1);
            socket.broadcast.emit('colors', colors);
            socket.username = username;
            socket.color = color;
            id = players.length;
            players.push({name: username, color: color, length: 3});

            socket.join(username);
            socket.currentBoard = username;
            var board = game_server.initBoard(username, color);


            socket.emit('login success', {
                players: players,
                username: username,
                snake: {position: {x: 0, y: 0}, direction: 'right', color: color},
                board: board
            });
            socket.broadcast.emit('user joined', {
                username: socket.username,
                players: players
            });


            /*EVENTS*/
            socket.on('snake move', function (snake, move) {
                snake.name = username;
                if (game_server.boards[socket.currentBoard]) {
                    game_server.boards[socket.currentBoard].snakes[username] = snake;
                    socket.to(socket.currentBoard).emit('snake move', snake, move);
                }
            });
            socket.on('i ate food', function (food) {
                updatePlayer(socket.username);
                io.sockets.emit('update score', players);
                var new_food = game_server.replace_food(socket.currentBoard, food);
                io.sockets.in(socket.currentBoard).emit('new food', new_food);
            });
            socket.on('i died', function (snake) {
                console.log('SNAKE DIED', snake);
                updatePlayer(socket.username, 1);
                io.sockets.emit('update score', players);
                socket.to(socket.currentBoard).emit('snake died', snake);
            });
            socket.on('teleport', function (snake) {
                snake.name = username;
                socket.leave(socket.currentBoard);
                socket.to(socket.currentBoard).emit('snake left', snake);
                delete game_server.boards[socket.currentBoard].snakes[username];
                snake = game_server.teleport(snake);
                /*new board*/
                var random_username = socket.currentBoard;
                if (players.length > 1) {
                    while (random_username == socket.currentBoard) {
                        random_username = players[Math.floor(Math.random() * players.length)].name;
                    }
                }
                var random_board = game_server.boards[random_username];
                socket.currentBoard = random_board.name;
                socket.emit('new board', snake, random_board);
                game_server.boards[socket.currentBoard].snakes[username] = snake;
                socket.to(socket.currentBoard).emit('snake joined', snake);
                socket.join(socket.currentBoard);
            });
            socket.on("disconnect", function (connection) {


                colors.push(socket.color);
                socket.to(socket.currentBoard).emit('snake left', {name: socket.username});
                console.log(game_server.boards[socket.currentBoard])
                delete game_server.boards[socket.currentBoard].snakes[username];
                console.log("===== disconnect " + username);
                for (var i in players) {
                    if (players[i].name == socket.username) {
                        players.splice(i, 1);
                    }
                }
                socket.broadcast.emit('user left', {
                    username: socket.username,
                    players: players
                });
            });

            socket.on('new message', function (data) {
                if (data == '/restart') {
                    io.sockets.emit('restart', socket.username);
                    players.forEach(function(p,i){
                       players[i].length = 3;
                    });
                    io.sockets.emit('update score', players);

                } else {
                    socket.broadcast.emit('new message', {
                        username: socket.username,
                        color: socket.color,
                        message: data
                    });
                }

            });
            socket.on('typing', function () {
                socket.broadcast.emit('typing', {
                    username: socket.username,
                    color: socket.color
                });
            });
            socket.on('stop typing', function () {
                socket.broadcast.emit('stop typing', {
                    username: socket.username
                });
            });
        }
    });
});

function updatePlayer(name, length) {
    players.forEach(function (p, i) {
        if (p.name == name) {
            players[i].length = typeof length !== 'undefined' ? length : players[i].length + 1;
        }
    });
    players.sort(function(a,b){
        if(a.length> b.length)return -1;
        if(a.length< b.length)return 1;
        return 0;
    });
}
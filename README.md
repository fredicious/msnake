
# Multiplayer Snake

Multiplayer Snake prototype using node.js and socket.io

## How to use

```
$ cd my-app
$ npm install
$ node server.js
```

And point your browser to `http://localhost:1337`. Optionally, specify
a port by supplying the `PORT` env variable.

## Features

Coming soon.

## Online demo

`http://www.fredicious.me:1337`
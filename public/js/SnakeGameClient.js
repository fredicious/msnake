var SnakeGame = (function (window) {

    function SnakeGame() {
        var canvas;
        var ctx;
        var board_w = 450;
        var board_h = 450;
        var socket;
        var cw = 10;
        var food;
        var my_snake;
        var board;
        var interval;
        var my_timeout;
        var new_directions = [];

        function init(soc, can, snake, b) {
            my_snake = snake;
            board = b;
            this.board = b;
            canvas = can;
            $(canvas).parent().attr('data-name', b.name+' room');
            $(canvas).parent().css('color', b.color);
            ctx = canvas.getContext("2d");
            ctx.fillStyle = board.color;
            ctx.fillRect(0, 0, board_w, board_h);
            socket = soc;
            this.add_food(board.food);
            my_snake.body = create_snake(my_snake);
            interval = 60;

            if (my_timeout != "undefined") clearTimeout(my_timeout);
            my_timeout = setTimeout(move_callback, 250);
            for (var s in b.snakes) {
                var sn = b.snakes[s];
                this.draw_snake(sn);
                if (sn.name != socket.username) {
                }
            }
        }

        function move_callback() {
            if(new_directions.length > 0){
                var new_direction = new_directions.shift();
                if (new_direction == "left" && my_snake.direction != "right") {
                    my_snake.direction = new_direction;
                }
                else if (new_direction == "up" && my_snake.direction != "down") {
                    my_snake.direction = new_direction;
                }
                else if (new_direction == "right" && my_snake.direction != "left") {
                    my_snake.direction = new_direction;
                }
                else if (new_direction == "down" && my_snake.direction != "up") {
                    my_snake.direction = new_direction;
                }
            }
            interval += 1;
            move();
            my_timeout = setTimeout(move_callback, interval);
        }

        this.board = board;
        this.init = init;
        this.add_food = function (f) {
            food = f;
            paint_cell(food.x, food.y, 'green');
        }
        this.stop = function () {
            if (my_timeout != "undefined") clearTimeout(my_timeout);
        }
        this.draw_snake = function (snake) {
            if (snake) {
                for (var c in snake.body) {

                    var cell = snake.body[c];
                    paint_cell(cell.x, cell.y, snake.color);
                }
            }
        }
        function erase_snake(snake) {
            if (snake) {
                for (var c in snake.body) {
                    var cell = snake.body[c];
                    erase_cell(cell.x, cell.y);
                }
            }
        }

        this.erase_snake = erase_snake;
        function exit_snake(snake) {
            if (snake) {
                for (var c = snake.body.length - 1; c >= 0; c--) {
                    var cell = snake.body[c];
                    (function (cell, c) {
                        setTimeout(function () {
                            erase_cell(cell.x, cell.y);
                        }, (snake.body.length - c) * 50);
                    })(cell, c);
                }
            }
        }

        this.exit_snake = exit_snake;
        function create_snake(snake) {
            var length = snake.body ? snake.body.length : 3; //Length of the snake
            var snake_array = []; //Empty array to start with
            for (var i = -1; i >= -length; i--) {
                var cell = {x: i, y: 0};
                if (snake.direction == "right") cell = {x: snake.position.x + i, y: snake.position.y};
                else if (snake.direction == "left") cell = {x: snake.position.x - i, y: snake.position.y};
                else if (snake.direction == "up") cell = {x: snake.position.x, y: snake.position.y - i};
                else if (snake.direction == "down") cell = {x: snake.position.x, y: snake.position.y + i};

                snake_array.push(cell);
            }
            return snake_array;
        }

        function move() {
            var move = {x: my_snake.body[0].x, y: my_snake.body[0].y}
            if (my_snake.direction == "right") move.x++;
            else if (my_snake.direction == "left") move.x--;
            else if (my_snake.direction == "up") move.y--;
            else if (my_snake.direction == "down") move.y++;

            if (check_collision(move, my_snake.body)) {
                socket.emit('i died', my_snake);
                die(my_snake);
                my_snake.body = [my_snake.body[0]];
                interval = 60;
                return;
            }
            paint(my_snake, move);
        }

        function paint(snake, move) {

            if (move.x == -1 || move.x == board_w / cw || move.y == -1 || move.y == board_h / cw) {
                exit_game(snake, move);
                return;
            }

            if (food && move.x == food.x && move.y == food.y) {
                var tail = {x: move.x, y: move.y};
                socket.emit('i ate food', food);
            }
            else {
                var tail = snake.body.pop(); //pops out the last cell
                erase_cell(tail.x, tail.y);
                tail.x = move.x;
                tail.y = move.y;
                paint_cell(tail.x, tail.y, my_snake.color);
            }

            snake.body.unshift(tail); //puts back the tail as the first cell
            socket.emit('snake move', snake, move);

        }

        function paint_cell(x, y, color) {
            ctx.fillStyle = color || 'grey';
            ctx.fillRect(x * cw, y * cw, cw, cw);
            ctx.strokeStyle = board.color;
            ctx.strokeRect(x * cw, y * cw, cw, cw);
        }

        function erase_cell(x, y) {
            ctx.fillStyle = board.color;
            ctx.fillRect(x * cw, y * cw, cw, cw);
            ctx.strokeStyle = board.color;
            ctx.strokeRect(x * cw, y * cw, cw, cw);
        }

        function check_collision(move, array) {
            var snakes = board.snakes;
            for (var i = 0; i < array.length; i++) {
                if (array[i].x == move.x && array[i].y == move.y)
                    return true;
            }
            var p = ctx.getImageData(move.x, move.y, 1, 1).data;
            for (s in snakes) {
                var sn = snakes[s];
                for (c in sn.body) {
                    cell = sn.body[c];
                    if (cell.x == move.x && cell.y == move.y) {
                        return true;
                    }
                }
            }
            return false;
        }

        function exit_game(snake, position) {
            erase_snake(my_snake);
            snake.position = position;
            if (my_timeout != "undefined") clearTimeout(my_timeout);
            socket.emit('teleport', snake);
        }

        function die(snake) {
            for (var c in snake.body) {

                var cell = snake.body[c];

                (function (cell, c) {
                    if (c > 0) {
                        setTimeout(function () {
                            paint_cell(cell.x, cell.y, 'grey');
                        }, c * 50);

                        setTimeout(function () {
                            erase_cell(cell.x, cell.y);
                        }, 50 * (snake.body.length - c) + snake.body.length * 50);
                    }
                })(cell, c);
            }
            ;
        }

        this.die = die;
        var move_timeout;
        $(document).keydown(function (e) {
            var key = e.which;

            if (key == "37" && my_snake.direction != "right" && my_snake.direction != "left") {
                e.stopPropagation();
                e.preventDefault();
                new_directions.push("left");
            }
            else if (key == "38" && my_snake.direction != "down" && my_snake.direction != "up") {
                e.stopPropagation();
                e.preventDefault();
                new_directions.push("up");
            }
            else if (key == "39" && my_snake.direction != "left" && my_snake.direction != "right") {
                e.stopPropagation();
                e.preventDefault();
                new_directions.push("right");
            }
            else if (key == "40" && my_snake.direction != "up" && my_snake.direction != "down") {
                e.stopPropagation();
                e.preventDefault();
                new_directions.push("down");
            }
            /*if (key == "37" && my_snake.direction != "right") move("left");
             else if (key == "38" && my_snake.direction != "down") move("up");
             else if (key == "39" && my_snake.direction != "left") move("right");
             else if (key == "40" && my_snake.direction != "up") move("down");*/
        })
    }

    return SnakeGame;

})(window);

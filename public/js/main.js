$(function () {
    var socket = io();
    var chat = new ChatModule(socket);
    var logger = new LogModule();
    var game = new SnakeGame();
    var canvases = [
        $("#gameCanvas"),
        $("#gameCanvas2")
    ];
    var active_canvas = 0;
    var $loginPage = $('.login'); // The login page
    var $input = $('#usernameInput');
    var color;
    var $chatPage = $('.chat'); // The chatroom page
    var $snakePicker = $('#snakePicker');
    var $scoreList = $('#scoreList');
    var players = [];
    //chat.setUsername('fred' + Date.now(), 'blue');
    socket.on('colors',function(colors){
        populateColors(colors);
        color = null;

        $snakePicker.find('>.snake').click(function (event) {
            var uname = $input.val();
            $snakePicker.find('>.snake').removeClass('selected');
            $(this).addClass('selected');
            color = $(this).attr('data-color') || 'blue';
            if (uname != '') {
                $input.removeClass('warning');
                chat.setUsername(uname, color);
            }
            else {
                $input.addClass('warning');
                $input.focus();
            }
        });
    });
    $input.keydown(function (event) {
        var uname = $input.val();
        if (event.which === 13 && uname != '' && color) {
            chat.setUsername(uname, color);
        }
    });
    socket.on('username taken', function (uname) {
        $input.popover('show');
        setTimeout(function(){$input.popover('destroy')},1500);
    });
    var default_snake, default_board;
    socket.on('login success', function (data) {
        $loginPage.fadeOut();
        $chatPage.show();
        console.log('Login Success', data);
        updateScore(data.players);
        socket.username = data.username;
        chat.init(data);
        var canvas = canvases[active_canvas][0];
        default_snake = data.snake;
        default_board = data.board;
        game.init(socket, canvas, data.snake, data.board);

        socket.on('restart', function(uname){
            logger.append('GAME RESTART by '+uname, 'white', 'grey');
            game.init(socket, canvas, default_snake, default_board);
        });
    });
    socket.on('snake move', function (snake, move) {
        //console.log('snake move', snake, move);
        var s = game.board.snakes[snake.name];
        game.erase_snake(s);
        game.board.snakes[snake.name] = snake;
        game.draw_snake(snake);
    });
    socket.on('snake joined', function (snake, move) {
        //console.log('snake joined', snake, game.board.snakes);
        logger.append(snake.name + ' joined room', snake.color);
        game.board.snakes[snake.name] = snake;
        game.draw_snake(snake);
    });
    socket.on('snake died', function (snake) {
        //console.log('snake died', snake);
        game.die(snake);
        snake.body = [snake.body[0]];
        game.board.snakes[snake.name] = snake;
    });
    socket.on('update score', function (players) {
        updateScore(players);
    });
    socket.on('snake left', function (snake, move) {
        logger.append(snake.name + ' left room', snake.color);
        //console.log('snake left', snake, game.board.snakes)
        var s = game.board.snakes[snake.name];
        game.exit_snake(s);
        delete game.board.snakes[snake.name];
    });
    socket.on('new food', function (food) {
        logger.append('new food available!', 'green');
        console.log('new food', food);
        game.add_food(food);
    });
    socket.on('new board', function (snake, board) {
        console.log('new board',snake, board)
        logger.append('You joined room '+board.name,'black', board.color);
        var old_convas = active_canvas;
        if (active_canvas == 0)active_canvas++;
        else active_canvas--;

        if (snake.direction == 'right') {
            var beforeOut = {x: '0%', y: '0%', duration: 0}
            var beforeIn = {x: '+100%', y: '0%', duration: 0}
            var tOut = {x: '-100%'};
            var tIn = {x: '0%'};
        }
        if (snake.direction == 'left') {
            var beforeOut = {x: '0%', y: '0%', duration: 0}
            var beforeIn = {x: '-100%', y: '0%', duration: 0}
            var tOut = {x: '+100%'};
            var tIn = {x: '0%'};
        }
        if (snake.direction == 'up') {
            var beforeOut = {y: '0%', x: '0%', duration: 0}
            var beforeIn = {y: '-100%', x: '0%', duration: 0}
            var tOut = {y: '+100%'};
            var tIn = {y: '0%'};
        }
        if (snake.direction == 'down') {
            var beforeOut = {y: '0%', x: '0%', duration: 0}
            var beforeIn = {y: '+100%', x: '0%', duration: 0}
            var tOut = {y: '-100%'};
            var tIn = {y: '0%'};
        }

        canvases[old_convas].parent().transition(beforeOut).transition(tOut).hide(0);
        canvases[active_canvas].parent().show(0).transition(beforeIn).transition(tIn);
        var canvas = canvases[active_canvas][0];
        game.init(socket, canvas, snake, board);

    });

    socket.on('new message', function (data) {
        chat.addChatMessage(data);
    });

    socket.on('user joined', function (data) {
        console.log('socket: user joined', data);
        chat.log(data.username + ' joined');
        chat.addParticipantsMessage(data);
        updateScore(data.players);
    });

    socket.on('user left', function (data) {
        chat.log(data.username + ' left');
        chat.addParticipantsMessage(data);
        chat.removeChatTyping(data);
    });

    socket.on('typing', function (data) {
        chat.addChatTyping(data);
    });

    socket.on('stop typing', function (data) {
        chat.removeChatTyping(data);
    });

    function populateColors(colors) {
        var snakes = [];
        colors.forEach(function (color) {
            var snake = $('<li class="snake" data-color="' + color + '"></li>');
            var body = [];
            for (var i = 1; i <= 3; i++) {
                body.push($('<span style="background-color: ' + color + '" class="snake-block"></span>'));
            }
            snake.append(body);
            snakes.push(snake);
        });
        $snakePicker.html('').append(snakes);
    }
    function updateScore(players) {
        var snakes = [];
        players.forEach(function (player) {
            var snake = $('<li class="snake">'+player.name+'</li>');
            snake.css('color', player.color);
            var body = [];
            for (var i = 1; i <= player.length; i++) {
                body.push($('<span style="background-color: ' + player.color + '" class="snake-block"></span>'));
            }
            snake.append(body);
            snakes.push(snake);
        });
        $scoreList.html('').append(snakes);
    }
});
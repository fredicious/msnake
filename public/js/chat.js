var ChatModule = (function (window) {


    function ChatModule(socket) {
        var FADE_TIME = 150; // ms
        var TYPING_TIMER_LENGTH = 400; // ms
        // Initialize varibles
        var $window = $(window);
        var $usernameInput = $('.usernameInput'); // Input for username
        var $messages = $('.messages'); // Messages area
        var $inputMessage = $('.inputMessage'); // Input message input box
        var connected = false;
        // Prompt for setting a username
        var username;
        var color;
        var typing = false;
        var lastTypingTime;
        var $currentInput = $usernameInput.focus();
        var socket;


        this.init = function init(data) {
            color = data.snake.color;
            connected = true;
            this.initEvents();
            $('#currentUser').html('Welcome '+data.username);
            this.addParticipantsMessage(data);
        };

        this.addParticipantsMessage = function addParticipantsMessage(data) {
            $('#numUsers').html(data.players.length);
        };

        // Sets the client's username
        this.setUsername = function setUsername(uname, color) {
            if (uname)username = this.cleanInput(uname.trim())
            else
                username = this.cleanInput($usernameInput.val().trim());

            // If the username is valid
            if (username) {
                // Tell the server your username
                socket.emit('login', username, color);
            }
        };

        // Sends a chat message
        this.sendMessage = function sendMessage() {
            var message = $inputMessage.val();
            // Prevent markup from being injected into the message
            message = this.cleanInput(message);
            // if there is a non-empty message and a socket connection
            if (message && connected) {
                $inputMessage.val('');
                this.addChatMessage({
                    username: username,
                    message: message,
                    color: color
                });
                // tell server to execute 'new message' and send along one parameter
                socket.emit('new message', message);
            }
        };

        // Log a message
        this.log = function log(message, options) {
            var $el = $('<li>').addClass('log').text(message);
            this.addMessageElement($el, options);
        };

        // Adds the visual chat message to the message list
        this.addChatMessage = function addChatMessage(data, options) {
            // Don't fade the message in if there is an 'X was typing'
            var $typingMessages = this.getTypingMessages(data);
            options = options || {};
            if ($typingMessages.length !== 0) {
                options.fade = false;
                $typingMessages.remove();
            }

            var $usernameDiv = $('<span class="username"/>')
                .text(data.username)
                .css('color', data.color);
            var $messageBodyDiv = $('<span class="messageBody">')
                .text(data.message);

            var typingClass = data.typing ? 'typing' : '';
            var $messageDiv = $('<li class="message"/>')
                .data('username', data.username)
                .addClass(typingClass)
                .append($usernameDiv, $messageBodyDiv);

            this.addMessageElement($messageDiv, options);
        };

        // Adds the visual chat typing message
        this.addChatTyping = function addChatTyping(data) {
            data.typing = true;
            data.message = 'is typing';
            this.addChatMessage(data);
        };

        // Removes the visual chat typing message
        this.removeChatTyping = function removeChatTyping(data) {
            this.getTypingMessages(data).fadeOut(function () {
                $(this).remove();
            });
        };

        this.addMessageElement = function addMessageElement(el, options) {
            var $el = $(el);

            if (!options) {
                options = {};
            }
            if (typeof options.fade === 'undefined') {
                options.fade = true;
            }
            if (typeof options.prepend === 'undefined') {
                options.prepend = false;
            }

            if (options.fade) {
                $el.hide().fadeIn(FADE_TIME);
            }
            if (options.prepend) {
                $messages.prepend($el);
            } else {
                $messages.append($el);
            }
            $messages[0].scrollTop = $messages[0].scrollHeight;
        };

        this.cleanInput = function cleanInput(input) {
            return $('<div/>').text(input).text();
        };

        this.updateTyping = function updateTyping() {
            if (connected) {
                if (!typing) {
                    typing = true;
                    socket.emit('typing');
                }
                lastTypingTime = (new Date()).getTime();

                setTimeout(function () {
                    var typingTimer = (new Date()).getTime();
                    var timeDiff = typingTimer - lastTypingTime;
                    if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                        socket.emit('stop typing');
                        typing = false;
                    }
                }, TYPING_TIMER_LENGTH);
            }
        }

        this.getTypingMessages = function getTypingMessages(data) {
            return $('.typing.message').filter(function (i) {
                return $(this).data('username') === data.username;
            });
        };

        this.initEvents = function initEvents() {
            var self = this;
            $window.keydown(function (event) {
                if (event.which === 13) {
                    if (username) {
                        self.sendMessage();
                        socket.emit('stop typing');
                        typing = false;
                    }
                }
            });

            $inputMessage.on('input', function () {
                self.updateTyping();
            });
        }
    }

    return ChatModule;

})(window);
var LogModule = (function (window) {


    function LogModule() {
        var $logList = $('#logList');
        this.append = function(message, color, bgcolor){
            var $el = $('<li>').addClass('log-element').text(message);
            if(color){
                $el.attr('style', 'color:'+color);
            }
            if(bgcolor){
                $el.attr('style', 'background:'+bgcolor);
            }
            $logList.append($el);
            $logList[0].scrollTop = $logList[0].scrollHeight;
        }
    }

    return LogModule;

})(window);
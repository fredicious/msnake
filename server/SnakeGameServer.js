var board_w = 450;
var board_h = 450;
var cw = 10;
var SnakeGameServer = {

    boards: [],

    initBoard: function (name, color) {

        var board = {
            food: SnakeGameServer.create_food(),
            name: name,
            snakes: {},
            color: lighten(color, 0.85)
        }
        SnakeGameServer.boards[name] = board;
        return board;
    },
    create_food: function () {
        return {
            x: Math.round(Math.random() * (board_w - cw) / cw),
            y: Math.round(Math.random() * (board_h - cw) / cw)
        };
    },
    replace_food: function (board, food) {
        var new_food = SnakeGameServer.create_food();
        SnakeGameServer.boards[board].food = new_food;
        return new_food;
    },
    teleport: function (snake) {
        var move = snake.position;
        var body = snake.body;
        for (c in body) {
            var cell = body[c];
            if (move.x < 0) {
                cell.x = board_w / cw + cell.x;
            } else if (move.y < 0) {
                cell.y = board_h / cw + cell.y;
            }

            if (move.x >= board_w / cw) {
                cell.x = cell.x - board_w / cw;
            }

            if (move.y >= board_h / cw) {
                cell.y = cell.y - board_h / cw;
            }
            body[c] = cell;
        }
        if(move.x < 0){
            move.x = board_w / cw + move.x;
        }else

        if(move.y < 0){
            move.y = board_h / cw + move.y;
        }

        if(move.x >= board_w / cw){
            move.x = move.x - board_w / cw;
        }

        if(move.y >= board_h / cw){
            move.y = move.y - board_h / cw;
        }
        snake.body = body;
        snake.position = move;
        return snake;
    }
};
function lighten(color, luminosity) {

    // validate hex string
    color = new String(color).replace(/[^0-9a-f]/gi, '');
    if (color.length < 6) {
        color = color[0]+ color[0]+ color[1]+ color[1]+ color[2]+ color[2];
    }
    luminosity = luminosity || 0;

    // convert to decimal and change luminosity
    var newColor = "#", c, i, black = 0, white = 255;
    for (i = 0; i < 3; i++) {
        c = parseInt(color.substr(i*2,2), 16);
        c = Math.round(Math.min(Math.max(black, c + (luminosity * white)), white)).toString(16);
        newColor += ("00"+c).substr(c.length);
    }
    return newColor;
}
module.exports = SnakeGameServer;